Overview
========

If you want to set up RADIUS authentication on lots of Junos devices, this may be useful to you.  It is even more useful if you are using Microsoft NPS as your RADIUS server, but you can ignore that part if you don't use NPS>


How it works
------------

NPS needs to be told what the RADIUS clients are (i.e. the Juniper devices that will be authenticating users), and also what the shared key is to encrypt the authentication requests and responses.  Creating a lot of these clients in the GUI (and doing it a second time on your redundant NPS) is quite tedious and prone to error.

This script creates commands using the 'netsh' command-line tool so that you can create the clients easily.  (The Powershell command New-NpsRadiusClient appears to be defective in that it somehow mangles the shared secret, forcing you to re-enter the secret in the GUI to make it work).   The netsh commands are printed to the screen so you can paste them into the command prompt on NPS.  The command looks like this:

> netsh nps add client name=SWITCH1 address=192.168.1.1 state=Enable sharedsecret=SECRET

Once pasted on to the NPS servers, refresh the RADIUS clients list in the GUI and you will see them appear.

After outputting this, the script proceeds to connect to each Juniper device in turn and put the configuration onto the live device.  It performs a 'show | compare' and prints that to the screen before asking if you want to commit the changes shown.  

At this point it is up to you to commit, or back out.  This stage looks like this:

    Connected to SWITCH1...
    
    [edit system]
    +  authentication-order [ radius password ];
    +  radius-server {
    +      172.16.1.1 {
    +          port 1812;
    +          secret "$9$QHiNONSENSEKMLXjHqfz69CuIRSApLNb2GU369Cu1"; ## SECRET-DATA
    +          retry 3;
    +          source-address 192.168.1.1;
    +      }
    +      172.16.1.2 {
    +          port 1812;
    +          secret "$9$2VoaUf5FNONSENSEUHPfzDipBEyW8oJGUHm"; ## SECRET-DATA
    +          retry 3;
    +          source-address 192.168.1.1;
    +      }
    +  }
    +  radius-options {
    +      password-protocol mschap-v2;
    +  }
    [edit system login]
    +    user SU {
    +        class super-user;
    +    }
    
    Do you want to commit (y/n):


A screenshot of the colourised version is below - in this case, the radius config was already present and only the secrets were being updated.

<img src="https://gitlab.com/amulheirn/radiusshizzle/-/raw/master/doc/shizzle.png">

Notes
=====

+ I do not tell you exactly how to set up NPS above, but the configuration for it creates the device names prefixed with 'NET-'.  This is so that NPS can match on the device using a 'Client Friendly Name' wildcard of NET-*

+ The NPS server must return a vendor-specific attribute to the Junos device that matches the locally-configured user called 'SU' that you see in the output above.  The RADIUS vendor code for Juniper is 2636, and the vendor-assigned attribute number is 1.  The value should be 'SU'

+ If you use PKI authentication, you can just enter the username when running the script, leaving the password blank.

### Disclaimer

The author accepts no responsibility for what you may do with this script. 