# Andrew Mulheirn - Feb 2020
# This script prints to screen the commands to create a lot of RADIUS clients in Microsoft NPS.
# This configuration must be manually pasted into a command prompt on the NPS servers.
#
# After that, it connects to each Juniper switch and applies the appropriate configuration
# with the right shared key, source IP address etc.

import csv
import getpass
from jnpr.junos import Device
from jnpr.junos.utils.config import Config
from jinja2 import Template
from colorama import Fore, init

init(autoreset=True)
data = {}
data['deviceUsername'] = input(Fore.GREEN + "Enter your username: ")
data['devicePassword'] = getpass.getpass(prompt=Fore.GREEN + "Enter your password: ")

# Read in Junos Configuration Template
with open('junos_config.j2') as junosConfig:
    configTemplate = Template(junosConfig.read())

# Read in Device List
with open('devices.csv') as csvfile:
    data['devices'] = list(csv.reader(csvfile))

print("RADIUS details - note that special characters (e.g. &, £, $) must be "
      "escaped for Microsoft's benefit using a backtick (`) character")

data['secret'] = input(Fore.GREEN + "Enter the RADIUS shared secret: ")
data['radiusServer1'] = input(Fore.GREEN + "Enter the first RADIUS server IP address: ")
data['radiusServer2'] = input(Fore.GREEN + "Enter the second RADIUS server IP address: ")

# strip any escape characters out of the secret so we don't apply then to Junos
data['junossecret'] = data['secret'].replace('`','')

print(f"{Fore.CYAN}======= NPS configuration ======")
for device in data['devices']:
    # NOTE - the Powershell command below somehow breaks the shared secret.
    # Even though you put it in right and when you do a Get-NpsRadiusCient it
    # shows up ok, it will not accept the RADIUS client until you go into the
    # GUI and type the shared secret in again!
    # print(f"Set-NpsRadiusClient -name {device[0]} -Address {device[1]} -SharedSecret {data['secret']}")
    # Instead, use the following netsh command
    print(f"{Fore.YELLOW}netsh nps add client name=NET-{device[0]} address={device[1]} state=Enable sharedsecret={data['secret']}")

print(f"{Fore.CYAN}================================")
print(f"{Fore.RED}Now please paste the above commands "
      "into a command prompt in your NPS servers")
print(f"{Fore.CYAN}===== END NPS configuration ====\n\n")





print(f"{Fore.CYAN}======= Junos Config =====\n")

for device in data['devices']:
    print(configTemplate.render(data=data))
    with Device(device[1], user=data['deviceUsername'], password=data['devicePassword'], port=22) as dev:
        print(f"{Fore.YELLOW}Connected to {device[0]}...")
        
        # Insert the variables into the template and store in configSnippet
        configSnippet = configTemplate.render(data=data, sourceIP=device[1])

        # Connect to the device and load the config
        cu = Config(dev)
        cu.load(configSnippet, format="set", merge=True)

        # Print to screen if desired
        #print(configTemplate.render(data=data, sourceIP=device[1]))

        # Perform a show | compare to check you didn't blat anything
        cu.pdiff()

        # Ask user if they want to commit or not
        goAhead = input(f"{Fore.RED}Do you want to commit (y/n): ")
        if goAhead == 'y':
            cu.commit(timeout=60)
        else:
            dev.close()
